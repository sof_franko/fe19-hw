import React from 'react';
import PropTypes from 'prop-types';
import './Modal.css'

class Modal extends React.Component {
    constructor(props) {
        super(props);

        this.onClose = this.onClose.bind(this);
    }

    onClose() {
        this.props.onClick()
    }

    render() {
        const modalDisplay = {
            display: this.props.show ? 'block' : 'none'
        }
        const modalHeaderBg = {
            background: this.props.style === 'success' ? 'olivedrab' : '#D44637'
        }
        const modalBodyBg = {
            background: this.props.style === 'success' ? 'darkolivegreen' : '#E74C3C'
        }

        return (
            <div style={modalDisplay} className='modal' onClick={this.onClose}>
                <div className='modal-dialog' onClick={e => e.stopPropagation()}>
                    <div className='modal-content'>
                        <div style={modalHeaderBg} className='modal-header'>
                            <h2>{this.props.header}</h2>
                            {this.props.closeBtn}
                        </div>
                        <div style={modalBodyBg} className='modal-body'>
                            {this.props.children}
                        </div>
                    </div>
                </div>
            </div>
        )

    }
}

Modal.PropType = {
    header: PropTypes.string,
    show: PropTypes.bool,
    closeBtn: PropTypes.bool
}

export default Modal