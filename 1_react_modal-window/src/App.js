import React from 'react';
import './App.css';
import Button from "./components/Button";
import Modal from "./components/Modal";

class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isSuccessModalShown: false,
            isErrorModalShown: false,
            closeButton: true
        }
        this.onSuccessModalShow = this.onSuccessModalShow.bind(this);
        this.onErrorModalShow = this.onErrorModalShow.bind(this);
        this.onDialogClose = this.onDialogClose.bind(this);
    }

    onSuccessModalShow() {
        this.setState({
            isSuccessModalShown: true
        })
    }

    onErrorModalShow() {
        this.setState({
            isErrorModalShown: true
        })
    }

    onDialogClose() {
        this.setState({
            isSuccessModalShown: false,
            isErrorModalShown: false
        })
    }

    render() {
        const closeBtnStyle = {
            cursor: 'pointer'
        }
        const closeBtn = this.state.closeButton
            ? <span style={closeBtnStyle} onClick={this.onDialogClose}>&times;</span>
            : '';

        return (
            <div className="container">
                <Button background='forestgreen' text='Open first modal' onClick={this.onSuccessModalShow}/>
                <Modal header='Your request is successful!'
                       show={this.state.isSuccessModalShown}
                       style={'success'}
                       closeBtn={closeBtn}
                       onClick={this.onDialogClose}
                >
                    <p>Everything it`s nice. Merry Christmas :3</p>
                    <Button background='darkgreen' text='Ok' onClick={this.onDialogClose}/>
                </Modal>

                <Button background='firebrick' text='Open second modal' onClick={this.onErrorModalShow}/>
                <Modal header='Do you want to delete this file?'
                       show={this.state.isErrorModalShown}
                       style={'error'}
                       closeBtn={closeBtn}
                       onClick={this.onDialogClose}
                >
                    <p>Once you delete this file, it won’t be possible to undo this action. Are you sure you
                        want to delete it?</p>
                    <Button background='#B3382C' text='Ok' onClick={this.onDialogClose}/>
                    <Button background='#B3382C' text='Cancel' onClick={this.onDialogClose}/>
                </Modal>
            </div>
        );
    }
}

export default App;
