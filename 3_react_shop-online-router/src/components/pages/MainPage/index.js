import {useState, useEffect} from 'react';
import Card from "../../items/Card/index";
import axios from "axios";
// import Request from "../../services/FetchRequests";

export default function MainPage({user}) {

    const [productsList, setProductsList] = useState([]);

    useEffect( async () => {
        const result = await axios(
            'https://hn.algolia.com/api/v1/search?query=redux',
        );
        console.log(result.data);
        setProductsList(result.data.hits)
    }, []);

    return (
        <>
            <h1>{user}, you are on the MainPage</h1>
            <div>
                Hello from mainPage/index.js.
                    <Card cardList={productsList}>
                        <div>
                            {/*<Button text='test' background='blue'/>*/}
                            {/*<Modal header='this is testing and nothing else'/>*/}
                        </div>
                    </Card>
                {/*{productsList.map(i => console.log(i))}*/}
                {/*{productsList.map(item =>*/}
                {/*)}*/}
            </div>

        </>
    )
}