import { Link } from 'react-router-dom'

function ToolBar({user}) {
    return (
        <div className="App">
            <Link to={`/`}>Main Page</Link> |
            <Link to={`/cart/`}>Cart <strong>{user.length}</strong></Link> |
            <Link to={`/favorites/`}>Favorites</Link>
        </div>

    )
}

export default ToolBar