export default async function Request(url) {
    return await fetch(url)
        .then(response => response.json());
}