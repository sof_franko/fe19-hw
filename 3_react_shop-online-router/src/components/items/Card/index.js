import React from "react";
import {useState} from 'react';
import propTypes from 'prop-types';
import './Card.css'

export default function Card({card}) {
    console.log(card)
    const {subTitle, title, price, imageUrl, vendorCode, color} = card;

    const [isFavorite, toggleFavoriteStatus] = useState(false);
    const addToFavorites = () => {
        toggleFavoriteStatus(!isFavorite);
        // addFavoriteClick(vendorCode, isFavorite);
    }

    const cardStyles = {
        backgroundColor: color
    }
    const favoriteBtnStyles = {
        backgroundColor: isFavorite ? 'hotpink' : 'blanchedalmond'
    }

    return (
        <div key={vendorCode} className='card-body'>
            <img src={imageUrl} alt={title}/>
            <div style={cardStyles} className='card-content'>
                <h3 key={subTitle}><i>{subTitle}</i></h3>
                <h2 key={title}>{title}</h2>
                <h3 key={price}>{price} &#x20b4;</h3>
            </div>
            <div className='btn_add-to-favorites' onClick={addToFavorites()}>
                <div style={favoriteBtnStyles} className='heart'> </div>
            </div>
        </div>

    )
}

Card.propTypes = {
    title: propTypes.string,
    subTitle: propTypes.string,
    price: propTypes.number,
    code: propTypes.number,
    imageUrl: propTypes.string,
    addFavoriteClick: propTypes.func
}