import React from 'react';
import propTypes from 'prop-types';
import './Button.css'

export default function Button({background, onClick, text}) {
    return (
        <button style={{background}} className='button' onClick={onClick()}>{text}</button>
    )
}

Button.propTypes = {
    onClick: propTypes.func,
    text: propTypes.string,
    background: propTypes.string
}