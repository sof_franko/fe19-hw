import {Switch, Route} from 'react-router-dom';
import MainPage from "../../pages/MainPage";
import CartPage from "../../pages/CartPage";
import FavoritesPage from "../../pages/FavoritesPage";

function DefaultRoute(props) {
    return (
        <Switch>
            <Route exact path='/' component={() => <MainPage {...props} />}/>
            <Route path='/cart' component={() => <CartPage {...props}/>}/>
            <Route exact path='/favorites' component={() => <FavoritesPage {...props}/>}/>
        </Switch>
    )
}

export default DefaultRoute;