import './App.css';
import DefaultRoute from "./components/layouts/DefaultRoute";
import ToolBar from "./components/ToolBar";
import {useState, useEffect} from 'react';

function App() {

    const [user, setNewUser] = useState({name: 'Sofiia'});

    return (
        <div className="App">
            <ToolBar user={user.name}/>
            <DefaultRoute user={user.name} />
        </div>
    );
}

export default App;
