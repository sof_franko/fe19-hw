import React from 'react'
import Card from "./components/Card";
import './components/Card/Card.css'

class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            productsList: [],
            favoriteProductsList: [],
            cartProductsList: []
        }
    }

    async componentDidMount() {
        const result = await fetch('./products/productsList.json')
            .then(resp => resp.json())
            .then(data => {
                console.log(data);
                this.setState({productsList: data})
            });

        const favouritesList = JSON.parse(localStorage.getItem('favoriteProductsList'));
        if (favouritesList) {
            this.setState({
                favoriteProductsList: favouritesList
            })
        }
        const cartList = JSON.parse(localStorage.getItem('cartProductsList'));
        if (cartList) {
            this.setState({
                cartProductsList: cartList
            })
        }
    }

    addFavoriteClick = (card, isFavorite) => {
        let clonedFav = [...this.state.favoriteProductsList];

        if (isFavorite) {
            const codeCardIndex = clonedFav.findIndex(code => code === card);
            clonedFav.splice(codeCardIndex, 1);
        } else {
            clonedFav.push(card)
        }

        this.setState({
            favoriteProductsList: clonedFav
        })
        localStorage.setItem('favoriteProductsList', JSON.stringify(clonedFav));
    }

    addCartClick = (card) => {
        let clonedFav = [...this.state.cartProductsList];
        clonedFav.push(card)

        this.setState({
            cartProductsList: clonedFav
        })
        localStorage.setItem('cartProductsList', JSON.stringify(clonedFav));
    }

    render() {
        return (
            <div className="cards-list">
                {this.state.productsList.map(item => <Card card={item}
                                                           addFavoriteClick={this.addFavoriteClick}
                                                           addCartClick={this.addCartClick}/>)}
            </div>
        );
    }
}

export default App;
