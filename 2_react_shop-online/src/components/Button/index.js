import React from 'react';
import propTypes from 'prop-types';
import './Button.css'

class Button extends React.Component {
    constructor(props) {
        super(props);
        this.onBtnShowClick = this.onBtnShowClick.bind(this);
    }

    onBtnShowClick() {
        this.props.onClick();
    }

    render() {
        const style = {
            background: this.props.background
        }
        return (
            <button style={style} className='button' onClick={this.onBtnShowClick}>{this.props.text}</button>
        )
    }
}

Button.propTypes = {
    onClick: propTypes.func,
    text: propTypes.string,
    background: propTypes.string
}

export default Button