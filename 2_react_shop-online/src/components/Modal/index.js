import React from 'react';
import propTypes from 'prop-types';
import './Modal.css'

class Modal extends React.Component {
    constructor(props) {
        super(props);

        this.onClose = this.onClose.bind(this);
    }

    onClose() {
        this.props.onClick()
    }

    render() {
        const modalDisplay = {
            display: this.props.show ? 'block' : 'none'
        }
        const closeBtn = this.props.closeBtn
            ? <span
                style={{cursor: "pointer", fontSize: 20 + "px"}}
                onClick={this.onClose}>&times;</span>
            : '';

        return (
            <div style={modalDisplay} className='modal' onClick={this.onClose}>
                <div className='modal-dialog' onClick={e => e.stopPropagation()}>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <h2>Do you want to add perfume <i>"{this.props.header}"</i> to the cart?</h2>
                            {closeBtn}
                        </div>
                        <div className='modal-body'>
                            {this.props.children}
                        </div>
                    </div>
                </div>
            </div>
        )

    }
}

Modal.defaultProps = {
    closeBtn: true
}

Modal.propTypes = {
    header: propTypes.string,
    show: propTypes.bool,
    closeBtn: propTypes.bool,
    onClick: propTypes.func
}

export default Modal