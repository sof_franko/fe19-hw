import React from "react";
import propTypes from 'prop-types';
import './Card.css'
import Button from "../Button";
import Modal from "../Modal";


class Card extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            isFavorite: false,
            isModalShown: false
        }
    }

    componentDidMount() {
        const favouritesList = JSON.parse(localStorage.getItem('favoriteProductsList'));
        if (favouritesList !== null && favouritesList.includes(this.props.card.vendorCode)) {
            this.setState({
                isFavorite: true
            })
        }
    }

    addToFavorites = () => {
        this.setState((state) => ({
            isFavorite: !state.isFavorite
        }));

        this.props.addFavoriteClick(this.props.card.vendorCode, this.state.isFavorite)
    }

    addToCart = () => {
        this.props.addCartClick(this.props.card.vendorCode)
        this.onModalClose()
    }


    onModalShown = () => {
        this.setState({
            isModalShown: true
        })
    }

    onModalClose = () => {
        this.setState({
            isModalShown: false
        })
    }

    render() {
        const {subTitle, title, price, imageUrl, vendorCode, color} = this.props.card;

        const cardStyles = {
            backgroundColor: color
        }
        const favoriteBtnStyles = {
            backgroundColor: this.state.isFavorite ? 'hotpink' : 'blanchedalmond'
        }

        return (
            <div key={vendorCode} className='card-body'>
                <img src={imageUrl} alt={title}/>
                <div style={cardStyles} className='card-content'>
                    <h3 key={subTitle}><i>{subTitle}</i></h3>
                    <h2 key={title}>{title}</h2>
                    <h3 key={price}>{price} &#x20b4;</h3>
                </div>
                <div className='btn_add-to-favorites' onClick={this.addToFavorites}>
                    <div style={favoriteBtnStyles} className='heart'> </div>
                </div>

                <div>
                    <Button text='Add to cart'
                        onClick={this.onModalShown}
                        background={color}/>
                    <Modal header={title}
                           show={this.state.isModalShown}
                           onClick={this.onModalClose}>
                        <p>Items settled to the basket will appear in the console below</p>
                        <Button background='darkgreen' text='Ok' onClick={this.addToCart}/>
                        <Button background='#B3382C' text='Cancel' onClick={this.onModalClose}/>
                    </Modal>
                </div>
            </div>
        )
    }
}

Card.propTypes = {
    title: propTypes.string,
    subTitle: propTypes.string,
    price: propTypes.number,
    code: propTypes.number,
    imageUrl: propTypes.string,
    addFavoriteClick: propTypes.func,
    addCartClick: propTypes.func
}

export default Card